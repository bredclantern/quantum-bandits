# Quantum Bandits

This repository contain a set of useful tools to conduct experiments using the quantum bandit setting,
and the QBAI algorithm.
(see arxiv : https://arxiv.org/abs/2002.06395 ).
